-- cria um novo schema
create schema agendat;

-- seleciona o banco de dados agendat
USE agendat;

-- cria a tabela contato
CREATE TABLE contato(
id BIGINT NOT NULL AUTO_INCREMENT,
nome VARCHAR(255),
email VARCHAR (255),
endereco VARCHAR (255),
dataNascimento DATE,
PRIMARY KEY(id)
);

-- consulta os dados da tabela contato
SELECT * FROM contato;