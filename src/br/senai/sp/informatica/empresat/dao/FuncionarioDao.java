package br.senai.sp.informatica.empresat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresat.model.Funcionario;

public class FuncionarioDao {

	// atributos
	private Connection connection;

	// contrutor
	public FuncionarioDao() {
		// esbalece um conexao com banco de dados
		connection = new ConnectionFactory().getConnection();
	}

	// salva
	public void salva(Funcionario funcionario) {

		// cria comando sql
		String sql = "INSERT INTO fucionario " + "(nome, email, senha) " + "VALUES(?,?,?,?)";

		try {
			// Cria um PreparedStatiment
			PreparedStatement stmt = connection.prepareStatement(sql);

			// cria os parametros de PreparedStatement
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getEmail());
			stmt.setString(3, funcionario.getSenha());

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// listar
	public List<Funcionario> getLista() {

		try {

			// cria um ArrayList
			List<Funcionario> funcionarios = new ArrayList<>();

			// cria um PreparedStatement
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM funcionairo");

			// executa o statement e guarda o resultado em um resultset
			ResultSet rs = stmt.executeQuery();

			// esquanto houver dados no resultset
			while (rs.next()) {
				// cria um contato com os dados do resultset
				Funcionario funcionario = new Funcionario();
				funcionario.setId(rs.getLong("id"));
				funcionario.setNome(rs.getString("nome"));
				funcionario.setEmail(rs.getString("email"));
				funcionario.setEmail(rs.getString("senha"));

				// adiciona o contato � lista de funcionario
				funcionarios.add(funcionario);
			} // fim do while

			// fecha o resultset
			rs.close();

			// fecha o statement
			stmt.close();

			// retorna lista de contatos
			return funcionarios;

		} catch (Exception e) {
			throw new RuntimeException();
		}

	}

}
