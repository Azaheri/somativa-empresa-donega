package br.senai.sp.informatica.empresat.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	public Connection getConnection() {
		try {
			// registra o driver jdbc
			Class.forName("com.mysql.jdbc.Driver");
			//retorna uma conex�o com o banco de dados
			//gerada pelo DriverManager
			return DriverManager.getConnection("jdbc:mysql://172.16.8.44/empresat", "tarde", "tarde");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
}
