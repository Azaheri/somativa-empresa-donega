package br.senai.sp.informatica.empresat.servelet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresat.dao.FuncionarioDao;
import br.senai.sp.informatica.empresat.model.Funcionario;

@WebServlet("/adicionaContato")
public class AdicionaFuncionarioServlet extends HttpServlet{
	
	//sobreescreve o metodo Sevice
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		// obtem um objeto do tipo PrintWriter do response
		//�til para escrever mensagens no navegador do usuario
		PrintWriter out = res.getWriter();
		
		
		//pega parametros do formulario
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String senha = req.getParameter("senha");
		
		
		
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setSenha(senha);
		
		//obtem uma instancia de ContatoDao
		FuncionarioDao dao = new FuncionarioDao();
		
		
		//sava contato no banco de dados
		dao.salva(funcionario);
		
		//feedback para o usuario
		out.println("<html>");
		out.println("<body>");
		out.println("Contato " + funcionario.getNome() + " salvo com sucesso!!!");
		out.println("</body>");
		out.println("</html>");
		
	}
	
}
